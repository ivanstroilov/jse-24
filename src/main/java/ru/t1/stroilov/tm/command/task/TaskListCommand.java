package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.model.Task;
import ru.t1.stroilov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Show all Tasks.";

    @NotNull
    public final static String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @Nullable final List<Task> tasks = getTaskService().findAll(getUserId(), sort);
        showTaskList(tasks);
        System.out.println("[END]");
    }
}
