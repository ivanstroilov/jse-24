package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;

public class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Delete all Tasks.";

    @NotNull
    public final static String NAME = "task-delete";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE TASKS]");
        getTaskService().deleteAll(getUserId());
        System.out.println("[TASKS DELETED]");
    }
}
